# conan-ios-cmake

conan package for this cmake ios toolchain file: 
https://github.com/leetal/ios-cmake

plus some more required parts taken from here:
https://github.com/theodelrieu/conan-darwin-toolchain

This package does not build fat binaries, but single arch ones.
However, conceptually, with an updated conan settings.yml, and some tweaks in the conaninfo.py file handling this settings extension, it will be possible to make fat binaries also.  