
from conans import ConanFile, tools

import os
import platform
import copy


class IosCMakeConan(ConanFile):
    name = "ios-cmake"
    version = "3.1.2"
    license = "BSD 3"
    settings = "os" , "arch"
    options = {
        "enable_bitcode": [True, False],
        "enable_arc": [True, False],
        "enable_visibility": [True, False],
        "enable_strict_try_compile": [True, False],
        "ios_target": ["OS", "OS64", "OS64COMBINED",
                       "SIMULATOR", "SIMULATOR64",
                       "TVOS", "TVOSCOMBINED",
                       "SIMULATOR_TVOS", "WATCHOS",
                       "WATCHOSCOMBINED", "SIMULATOR_WATCHOS"]
    }
    default_options = {
        "enable_bitcode": False,
        "enable_arc": False,
        "enable_visibility": False,
        "enable_strict_try_compile": False,
        "ios_target": "OS64COMBINED",
    }
    description = "ios Cmake toolchain to (cross) compile macOS/iOS/watchOS/tvOS"
    url = "https://github.com/leetal/ios-cmake"
    exports_sources = "ios.toolchain.cmake", "cmake-wrapper"


# TODO ? limit to values like here in the android ?
    # settings = {"os_build": ["Macos"],
    #             "arch_build": ["x86_64"],
    #             "compiler": ["clang"], # apple-clang
    #             "os": ["iOS"],
    #             "arch": ["x86_64", "armv7", "armv7s", "armv8"] # missing tv/watch
    # }

    @staticmethod
    def _chmod_plus_x(filename):
        if os.name == 'posix':
            os.chmod(filename, os.stat(filename).st_mode | 0o111)

    def source(self):
        self.run("git clone https://github.com/leetal/ios-cmake.git")

    def build(self):
        pass

    def package(self):
        self.copy("ios-cmake/ios.toolchain.cmake")
        dir_path = os.path.dirname(os.path.realpath(__file__))
        os.path.join(dir_path, "cmake-wrapper")
        self.copy(os.path.join(dir_path, "cmake-wrapper"), "cmake-wrapper")
        self.copy("cmake-wrapper")

    def package_info(self):

        arch_flag = self.settings.arch
        if arch_flag == "armv8":
            arch_flag = "arm64"


        cmake_options = "-DENABLE_BITCODE={} -DENABLE_ARC={} -DENABLE_VISIBILITY={} -DENABLE_STRICT_TRY_COMPILE={}".format(
            self.options.enable_bitcode, 
            self.options.enable_arc, 
            self.options.enable_visibility,
            self.options.enable_strict_try_compile  
        ) 
        cmake_flags = "-DPLATFORM={} -DDEPLOYMENT_TARGET={} -DARCHS={} {}".format(
            self.options.ios_target, self.settings.os.version, arch_flag, cmake_options
        ) 

        self.env_info.CONAN_USER_CMAKE_FLAGS = cmake_flags
        self.output.info('Setting toolchain options to: %s' % cmake_flags)
        cmake_wrapper = os.path.join(self.package_folder, "cmake-wrapper")
        self._chmod_plus_x(cmake_wrapper)
        self.output.info('Setting CONAN_CMAKE_PROGRAM to: %s' % cmake_wrapper)
        self.env_info.CONAN_CMAKE_PROGRAM = cmake_wrapper
        tool_chain = os.path.join(self.package_folder,
                                  "ios-cmake",
                                  "ios.toolchain.cmake"
                                  )
        self.env_info.CONAN_CMAKE_TOOLCHAIN_FILE = tool_chain

    def package_id(self):
        self.info.header_only()
